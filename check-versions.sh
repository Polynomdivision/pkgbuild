#!/bin/bash

# Returns the value of ann abitrary variable from the PKGBUILD
# $1: Path to PKGBUILD
# $2: Attribute (e.g 'pkgver' for the value of 'pkgver')
get_from_pkgbuild() {
	grep --color=never "$2\=" $1 | sed -e "s/$2\=//" -e "s/'//g" -e 's/"//g'	
}

# Returns the value of 'pkgver' from the file $1
# $1: Path to file
get_package_version() {
	get_from_pkgbuild $1 pkgver	
}

# Download a file
# $1: Output file name
# $2: URL
download() {
	wget -O $1 \
		$2 \
		&> /dev/null
	if [ ! $? = 0 ]; then
		echo "An error occurred during download"
		exit 1
	fi
}

info() {
	echo "[$pkg] $1"
}
info_n() {
	echo -n "[$pkg] $1"
}

# Checks if the upstream PKGBUILD has a newer version
# $1: Pkgname
# $2: Upstream PKGBUILD url
perform_check() {
	pkg=$1
	info_n "Checking..."
	download $tmp/$pkg $2 
	locver=$(get_package_version $pkg/PKGBUILD)
	remver=$(get_package_version $tmp/$pkg)
	if [ ! $locver = $remver ]; then
		echo " New version found"
		info "Local: $locver"
		info "Remote: $remver"
	else
		echo " Up to date"
	fi
}

# Compare the Github release tag and the local tag
# NOTE: This assumes that jq -r '.[0]' returns the newest tag
# $1: Pkgname
perform_github_tags_check() {
	pkg=$1
	url=$(get_from_pkgbuild $1/PKGBUILD url)
	locsha=$(get_from_pkgbuild $1/PKGBUILD release_sha)

	info_n "Checking..."
	
	# Query the Github API
	owner_repo=$(echo $url | sed -e 's/https\:\/\/github\.com\///')
	api_data=$(curl --silent https://api.github.com/repos/$owner_repo/tags)
	remsha=$(echo $api_data | jq -r '.[0].commit.sha')
	tag=$(echo $api_data | jq -r '.[0].name')
	if [ ! "$remsha" = "$locsha" ]; then
		echo " New version found"
		info "Local: Based on commit $locsha"
		info "Remote: $remsha ($tag)"
	else
		echo " Up to date"
	fi
}

tmp=$(mktemp --directory)

# Check the packages
perform_check nvidia-custom 'https://git.archlinux.org/svntogit/packages.git/plain/trunk/PKGBUILD?h=packages/nvidia'
perform_check breeze-cursor 'https://git.archlinux.org/svntogit/packages.git/plain/trunk/PKGBUILD?h=packages/breeze'
perform_check primus 'https://git.archlinux.org/svntogit/community.git/plain/trunk/PKGBUILD?h=packages/primus'
perform_github_tags_check scream-pulse
perform_check lutris-custom 'https://git.archlinux.org/svntogit/community.git/plain/trunk/PKGBUILD?h=packages/lutris'

rm -rf $tmp
